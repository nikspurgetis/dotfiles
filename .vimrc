call plug#begin()
Plug 'chriskempson/base16-vim'
Plug 'easymotion/vim-easymotion'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'majutsushi/tagbar'
Plug 'tommcdo/vim-fubitive'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-surround'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-syntastic/syntastic'
call plug#end()


" General settings
" Many defaults handily provided by tpope's vim-sensible plugin!
set cursorline
set hidden
set hlsearch
set number

imap jj <Esc>


" base16-vim settings
if filereadable(expand("~/.vimrc_background"))
	let base16colorspace=256
	source ~/.vimrc_background
endif


" fzf.vim settings
nmap <C-P> :FZF<CR>


" syntastic settings
let g:syntastic_java_checkers=['javac']
let g:syntastic_java_javac_config_file_enabled = 1


" tagbar settings
nmap <F8> :TagbarToggle<CR>


" vim-airline settings
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#fnamemod=':t'

